# 概要
このプロジェクトはJVMで動くrubyのjrubyを使ったプロジェクト管理の一例です。
特定のjavaライブラリを使うことを前提に、通常のruby管理ではなく、gradleによって管理し、自動的にgemとjavaライブラリの依存を解決します。

## チートシート

* テスト実行する
`
gradle jrubyRun
`
* jarファイルにビルドする
`
gradle jrubyJar
`
* ビルドファイルを削除する
`
gradle clean
`
* ビルドしたものを実行する
`
java -jar build/libs/{プロジェクト名}-jruby.jar
`
