## ruby gem
require "colorize"
## java libs
java_import "org.glycoinfo.WURCSFramework.util.validation.WURCSValidator"

##java code in jruby
puts "#######WFWLOGS-START#######"
validator = WURCSValidator.new()
validator.start("WURCS=2.0/1,1,0/[u2122h]/1/")
puts "#######WFWLOGS-END#########"
p validator.getReport().input_string
p validator.getReport().standard_string
p validator.getReport().getResults()

## ruby code
puts "Roses are red".red
puts "Violets are blue".blue
puts "I can use JRuby/Gradle".green
puts "And now you can too!".yellow
